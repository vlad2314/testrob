﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json.Linq;
using System.Net.Sockets;
using System.Text;
using System.Diagnostics;
using Newtonsoft.Json;
using System.Windows;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Controls;

namespace TestRob
{
    class ServerConnect
    {
        private static TcpClient client = new TcpClient();
        private static NetworkStream stream;
        private static Thread clientThreadRead;
        private static Thread clientThreadWrite;


        private static string JsonInput;
        public static InputData data;

        [Serializable]
        public struct InputData
        {
            public string namePC;
            public string IPPC;
            public string ProcessorPC;
            public string VideocardPC;
            public string MemoryPC;
            public string Motherboard;
            public string ScreenPC;
        }

        // Start is called before the first frame update
        // При запуске программы должен быть вызван StartClient()
        public static void Start()
        {
            StartClient();
            Write("GetParameters");


        }
        

        private static void StartClient()
        {
            try
            {
                string serverAddr = "127.0.0.1";
                int serverPort = 13000;
                client.Connect(serverAddr, serverPort);
                if (client.Connected)
                {
                    stream = client.GetStream();

                    clientThreadRead = new Thread(new ThreadStart(Read));
                    clientThreadRead.Priority = ThreadPriority.Lowest;
                    clientThreadRead.Start();
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                //MessageBox.Show(ex.Message);
                MessageBox.Show(ex.Message);
            }

        }

        // Бесконечно читает полученные сообщения

        [STAThread]
        [Obsolete]
        public static void Read()
        {
            try
            {
                while (true)
                {
                    byte[] dataRead = new byte[256];
                    int bytesRead = 0;
                    do
                    {
                        bytesRead = stream.Read(dataRead, 0, dataRead.Length);
                        string message = (Encoding.UTF8.GetString(dataRead, 0, bytesRead)).ToString();
                        if (message.IndexOf('{') != -1) // Это JSON
                        {
                            JsonInput = message;
                            Write();
                        }
                       
                        Console.WriteLine(message);
                    }
                    while (stream.DataAvailable);

                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }

        private static void Write()
        {
            data = JsonConvert.DeserializeObject<InputData>(JsonInput); // Newtonsoft.Json.JsonConvert.DeserializeObject<InputData>(JsonInput);
           
            Console.WriteLine(JsonInput);
           
        }

        // Отправить сообщение в любое время
        public static void Write(string messange)
        {
            try
            {
                byte[] data = new byte[256];
                stream = client.GetStream();
                data = Encoding.UTF8.GetBytes(messange);
                stream.Write(data, 0, data.Length);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                //stream.Close();
                //client.Close();
            }

        }
        public static void WriteLine()
        {

            JObject jsonParams = new JObject();
            jsonParams.Add("namePC", data.namePC);
            jsonParams.Add("IPPC", data.IPPC);
            jsonParams.Add("ProcessorPC", data.ProcessorPC);
            jsonParams.Add("VideocardPC", data.VideocardPC);
            jsonParams.Add("MemoryPC", data.MemoryPC);
            jsonParams.Add("Motherboard", data.Motherboard);
            Write(JsonConvert.SerializeObject(jsonParams));
        }
        public static bool ExitClient()
        {
            client.Close();
            stream.Dispose();
            return true;
        }


    }

}
