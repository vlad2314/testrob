﻿using System;
using System.Windows;
using System.Management;
using System.Collections.Generic;
using System.Net;
using System.Drawing;
using System.Windows.Forms;
using System.Windows.Media.Imaging;

namespace TestRob
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private void GameView_Loaded(object sender, RoutedEventArgs e)
        {
            PC.Text =
                Properties.Resources.namePC.ToString() +"\n" +
                Properties.Resources.IPPC.ToString() + "\n" +
                Properties.Resources.ProcessorPC.ToString() + "\n" +
                Properties.Resources.VideocardPC.ToString() + "\n" +
                Properties.Resources.MemoryPC.ToString() + "\n" +
                Properties.Resources.Motherboard.ToString();
            Loaded -= GameView_Loaded;
        }
        public MainWindow()
        {
            InitializeComponent();
            Loaded += GameView_Loaded;
            
        }
        private static List<string> GetHardwareInfo(string WIN32_Class, string ClassItemField)
        {
            ObjectQuery winQuery = new ObjectQuery("SELECT * FROM " + WIN32_Class);
            List<string> result = new List<string>();

            ManagementObjectSearcher searcher = new ManagementObjectSearcher(winQuery);

            try
            {
                foreach (ManagementObject obj in searcher.Get())
                {
                    result.Add(obj[ClassItemField].ToString().Trim());
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return result;
        }
        private string OutputResult(List<string> result)
        {
            string res = null;
            if (result.Count > 0)
            {
                for (int i = 0; i < result.Count; ++i)
                    res = result[i];
            }
            return res;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            PC.Text =
                Properties.Resources.namePC.ToString() + Environment.MachineName + "\n" +
                Properties.Resources.IPPC.ToString() + Dns.GetHostByName(Environment.MachineName).AddressList[0] + "\n" +
                Properties.Resources.ProcessorPC.ToString() + OutputResult(GetHardwareInfo("Win32_Processor", "Name")) + "\n" +
                Properties.Resources.VideocardPC.ToString() + OutputResult(GetHardwareInfo("Win32_VideoController", "Name")) + "\n" +
                Properties.Resources.MemoryPC.ToString() + OutputResult(GetHardwareInfo("Win32_LogicalMemoryConfiguration", "TotalPhysicalMemory"))+ "\n" +
                Properties.Resources.Motherboard.ToString() + OutputResult(GetHardwareInfo("Win32_BaseBoard", "SerialNumber"));
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            Bitmap printscreen = new Bitmap(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height);
            Graphics graphics = Graphics.FromImage(printscreen as Image);
            graphics.CopyFromScreen(0, 0, 0, 0, printscreen.Size);
            printscreen.Save("1.jpeg", System.Drawing.Imaging.ImageFormat.Jpeg);
            PCScreen.Source = new BitmapImage(new Uri("pack://siteoforigin:,,,/1.jpeg"));
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {

            ServerConnect.Start();
            ServerConnect.data.namePC = Environment.MachineName;
            ServerConnect.data.IPPC = Dns.GetHostByName(Environment.MachineName).AddressList[0].ToString();
            ServerConnect.data.ProcessorPC = OutputResult(GetHardwareInfo("Win32_Processor", "Name"));
            ServerConnect.data.VideocardPC = OutputResult(GetHardwareInfo("Win32_VideoController", "Name"));
            ServerConnect.data.MemoryPC = OutputResult(GetHardwareInfo("Win32_LogicalMemoryConfiguration", "TotalPhysicalMemory"));
            ServerConnect.data.Motherboard = OutputResult(GetHardwareInfo("Win32_BaseBoard", "SerialNumber"));
            ServerConnect.WriteLine();
        }
    }
}
